<?php get_header();
	/**
 * The template for displaying all single posts and attachments

 */
 ?>


	<div id="primary" class="content-area container">
<div class="row">
	<div class="col-12">


				<!-- start breadcrumbs -->
				<?php
				if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb( '<p id="breadcrumb">','</p>' );
				}
				?>

	</div>




  <!-- Content here -->
	<?php
    // Start the loop.
    while (have_posts()) : the_post();
    ?>
<div class="col-12">
	<h1>	<?php the_title(); ?></h1>
</div>

</div>


  <div class="row">
		<div class="col">
		<?php
        the_content();
        ?>
	</div>
  </div>

<?php
// End of the loop.
endwhile;
?>
</div>
<?php get_footer(); ?>
